/*
 * This file is part of LibKGAPI library
 *
 * SPDX-FileCopyrightText: 2018 Daniel Vrátil <dvratil@kde.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#ifndef LIBKGAPI2_ENUMS_H_
#define LIBKGAPI2_ENUMS_H_

#include "kgapicalendar_export.h"

#include <QObject>

namespace KGAPI2 {

#if QT_VERSION >= QT_VERSION_CHECK(5, 14, 0)
Q_NAMESPACE_EXPORT(KGAPICALENDAR_EXPORT)
#else
KGAPICALENDAR_EXPORT Q_NAMESPACE
#endif

/**
 * Determines whether Google Calendar should send updates to participants
 */
enum class SendUpdatesPolicy
{
    All,    /// Send updates to all attendees
    ExternalOnly, /// Only send updates to non-Google Calendar participants
    None, /// Don't send any notifications
};

Q_ENUM_NS(KGAPI2::SendUpdatesPolicy)

} // namespace KGAPI

#endif // LIBKGAPI2_ENUMS_H_
