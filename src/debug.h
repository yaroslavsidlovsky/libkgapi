/*
    SPDX-FileCopyrightText: 2013 Daniel Vrátil <dvratil@redhat.com>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/

#ifndef LIBKGAPI2_DEBUG_H
#define LIBKGAPI2_DEBUG_H

#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(KGAPIDebug)

#endif // LIBKGAPI2_DEBUG_H
